use arbitrary::{Arbitrary, Result, Unstructured};

use crate::{Domain, GenericRange, Ranges};

impl<'data, T: Arbitrary<'data> + Domain> Arbitrary<'data> for Ranges<T> {
    #[allow(clippy::min_ident_chars)]
    fn arbitrary(u: &mut Unstructured<'data>) -> Result<Self> {
        let len = u.arbitrary_len::<GenericRange<T>>()?;
        let mut vec = Self::with_capacity(len);

        for _ in 0..len {
            let _: bool = vec.insert(u.arbitrary::<GenericRange<T>>()?);
        }

        Ok(vec)
    }
}
