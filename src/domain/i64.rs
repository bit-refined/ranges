use core::{cmp::Ordering, ops::Bound};

use super::{Domain, Iterable};

impl Domain for i64 {
    const DISCRETE: bool = true;

    /// Always returns `Some(self - 1)` unless `self` is `Self::MIN`.
    #[must_use]
    #[allow(clippy::arithmetic_side_effects)]
    fn predecessor(&self) -> Option<Self> {
        match *self {
            Self::MIN => None,
            _ => Some(self - 1),
        }
    }

    /// Always returns `Some(self + 1)` unless `self` is `Self::MAX`.
    #[must_use]
    #[allow(clippy::arithmetic_side_effects)]
    fn successor(&self) -> Option<Self> {
        match *self {
            Self::MAX => None,
            _ => Some(self + 1),
        }
    }

    /// Returns `Included(Self::MIN)`.
    #[must_use]
    fn minimum() -> Bound<Self> {
        Bound::Included(Self::MIN)
    }

    /// Returns `Included(Self::MAX)`.
    #[must_use]
    fn maximum() -> Bound<Self> {
        Bound::Included(Self::MAX)
    }

    #[must_use]
    #[allow(clippy::shadow_reuse, clippy::arithmetic_side_effects, clippy::as_conversions)]
    fn shares_neighbour_with(&self, other: &Self) -> bool {
        let (big, small) = match self.cmp(other) {
            Ordering::Less => (other, self),
            Ordering::Equal => return false,
            Ordering::Greater => (self, other),
        };

        #[allow(clippy::cast_sign_loss)]
        let big = (big ^ Self::MIN) as u64;
        #[allow(clippy::cast_sign_loss)]
        let small = (small ^ Self::MIN) as u64;

        big - small == 2
    }
}

impl Iterable for i64 {
    type Output = Self;

    fn next(&self) -> Option<Self::Output> {
        if *self == Self::MAX {
            None
        } else {
            #[allow(clippy::arithmetic_side_effects)]
            Some(*self + 1)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::Domain;

    #[test]
    fn is_next_to() {
        assert!(i64::MIN.is_next_to(&(i64::MIN + 1)));
        assert!((i64::MIN + 1).is_next_to(&i64::MIN));
        assert!(!i64::MIN.is_next_to(&(i64::MIN + 2)));
        assert!(!i64::MIN.is_next_to(&i64::MAX));
        assert!(!i64::MAX.is_next_to(&i64::MIN));
    }

    #[test]
    fn shares_neighbour_with() {
        // self-distance
        assert!(!i64::MIN.shares_neighbour_with(&i64::MIN));

        // "normal" value
        assert!(!42_i64.shares_neighbour_with(&45));
        assert!(!45_i64.shares_neighbour_with(&42));

        assert!(42_i64.shares_neighbour_with(&44));
        assert!(44_i64.shares_neighbour_with(&42));

        // boundary check
        assert!(!i64::MIN.shares_neighbour_with(&i64::MAX));
        assert!(!i64::MAX.shares_neighbour_with(&i64::MIN));
    }
}
